namespace libCalculadoraUC
{
    public class Operacion
    {
        private int num1, num2;

        public Operacion(int num1, int num2)
        {
            this.num1 = num1;
            this.num2 = num2;
        }
        public int Suma()
        {
            return num1 + num2;
        }
        public int Resta()
        {
            return num1 - num2;
        }
    }
}
